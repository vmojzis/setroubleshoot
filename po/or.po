# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
#
# Translators:
# Dimitris Glezos <glezos@indifex.com>, 2011
# Manoj Kumar Giri <giri.manojkr@gmail.com>, 2008
# Manoj Kumar Giri <mgiri@redhat.com>, 2008-2013
# Manoj Kumar Giri <mgiri@redhat.com>, 2013
# Subhransu Behera <arya_subhransu@yahoo.co.in>, 2007
# Subhransu Behera <sbehera@redhat.com>, 2006
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-09-07 13:59+0200\n"
"PO-Revision-Date: 2017-08-31 08:30-0400\n"
"Last-Translator: Copied by Zanata <copied-by-zanata@zanata.org>\n"
"Language-Team: Oriya (http://www.transifex.com/projects/p/fedora/language/"
"or/)\n"
"Language: or\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Zanata 4.6.2\n"

msgid "SELinux Troubleshooter"
msgstr "SELinux ତ୍ରୁଟିନିବାରକ"

msgid "Troubleshoot SELinux access denials"
msgstr "ତ୍ରୁଟି ନିବାରକ SELinux ପ୍ରବେଶ ନିଷିଦ୍ଧ"

msgid "policy;security;selinux;avc;permission;mac;alert;sealert;"
msgstr "ନିତୀ;ସୁରକ୍ଷା;selinux;avc;ଅନୁମତି;mac;ସତର୍କ ସୂଚନା;sealert;"

#, python-format
msgid "port %s"
msgstr "ପୋର୍ଟ %s"

msgid "Unknown"
msgstr "ଅଜଣା"

#, python-format
msgid ""
"%s \n"
"**** Recorded AVC is allowed in current policy ****\n"
msgstr ""

#, python-format
msgid ""
"%s \n"
"**** Recorded AVC is dontaudited in current policy. 'semodule -B' will turn "
"on dontaudit rules ****\n"
msgstr ""

msgid "Must call policy_init first"
msgstr "ନିଶ୍ଚିତ ଭାବରେ policy_init କୁ ପ୍ରଥମେ ଡାକନ୍ତୁ"

#, python-format
msgid ""
"%s \n"
"**** Invalid AVC: bad target context ****\n"
msgstr ""

#, python-format
msgid ""
"%s \n"
"**** Invalid AVC: bad source context ****\n"
msgstr ""

#, python-format
msgid ""
"%s \n"
"**** Invalid AVC: bad type class ****\n"
msgstr ""

#, python-format
msgid ""
"%s \n"
"**** Invalid AVC: bad permission ****\n"
msgstr ""

msgid "Error during access vector computation"
msgstr "ଭେକ୍ଟର ଗଣନାରେ ଅଭିଗମ୍ୟ କରିବା ସମୟରେ ତ୍ରୁଟି"

msgid "SELinux Alert Browser"
msgstr "SELinux ସତର୍କ ସୂଚନା ବ୍ରାଉଜର"

msgid "The source process:"
msgstr "ଉତ୍ସ ପଦ୍ଧତି:"

msgid "Yes"
msgstr "ହଁ"

msgid "No"
msgstr "ନାଁ"

msgid "Attempted this access:"
msgstr "ଏହି ଅଭିଗମ୍ୟତାକୁ ପ୍ରୟାସ କରାଯାଇଛି:"

msgid "SETroubleshoot Details Window"
msgstr "SETroubleshoot ବିବରଣୀ ୱିଣ୍ଡୋ"

msgid "Would you like to receive alerts?"
msgstr "ସତର୍କ ସୂଚନା ଗ୍ରହଣ କରିବା ପାଇଁ ଚାହୁଁଛନ୍ତି କି?"

msgid "Notify Admin"
msgstr "ପ୍ରଶାସକଙ୍କୁ ସୂଚାନ୍ତୁ"

msgid "Troubleshoot"
msgstr "ତ୍ରୁଟି ନିବାରଣ କରନ୍ତୁ"

msgid "Details"
msgstr "ବିସ୍ତୃତ ବିବରଣୀ"

msgid "SETroubleshoot Alert List"
msgstr "SETroubleshoot ଚେତାବନୀ ତାଲିକା"

msgid "List All Alerts"
msgstr "ସମସ୍ତ ସତର୍କ ସୂଚନାକୁ ତାଲିକାଭୁକ୍ତ କରନ୍ତୁ"

msgid "#"
msgstr "#"

msgid "Source Process"
msgstr "ଉତ୍ସ ପଦ୍ଧତି"

msgid "Attempted Access"
msgstr "ପ୍ରୟାସ କରାଯାଇଥିବା ଅଭିଗମ୍ୟତା"

msgid "On this"
msgstr "ଏହା ଉପରେ"

msgid "Occurred"
msgstr "ଘଟିଛି"

msgid "Last Seen"
msgstr "ଅନ୍ତିମ ସାକ୍ଷାତ"

msgid "Status"
msgstr "ଅବସ୍ଥିତି"

#, fuzzy, python-format
#| msgid "Unable to grant access."
msgid ""
"Unable to notify admin.\n"
"\n"
"%s"
msgstr "ଅଭିଗମ୍ୟତା ଅନୁମତି ପାଇବାରେ ଅସମର୍ଥ।"

msgid "Notify"
msgstr "ସୂଚାନ୍ତୁ"

msgid "Notify alert in the future."
msgstr "ଭବିଷ୍ୟତରେ ସତର୍କ ସୂଚନା ଦିଅନ୍ତୁ।"

msgid "Ignore"
msgstr "ଆଗ୍ରହ୍ଯ କରନ୍ତୁ"

msgid "Ignore alert in the future."
msgstr "ଭବିଷ୍ୟତରେ ସତର୍କ ସୂଚନାକୁ ଅଗ୍ରାହ୍ୟ କରନ୍ତୁ।"

msgid "<b>If you were trying to...</b>"
msgstr "<b>ଯଦି ଆପଣମାନେ ଚେଷ୍ଟା କରୁଛନ୍ତି...</b>"

msgid "<b>Then this is the solution.</b>"
msgstr "<b>ତେବେ ଏହା ହେଉଛି ତାହାର ସମାଧାନ।</b>"

msgid ""
"Plugin\n"
"Details"
msgstr ""
"ପ୍ଲଗଇନ\n"
"ବିବରଣୀ"

msgid ""
"Report\n"
"Bug"
msgstr ""
"ତ୍ରୁଟିକୁ\n"
"ଖବର କରନ୍ତୁ"

#, python-format
msgid "Plugin: %s "
msgstr "ପ୍ଲଗଇନ: %s "

msgid "Unable to grant access."
msgstr "ଅଭିଗମ୍ୟତା ଅନୁମତି ପାଇବାରେ ଅସମର୍ଥ।"

#, python-format
msgid "Alert %d of %d"
msgstr "ଚେତାବନୀ %d ର %d"

#, python-format
msgid "On this %s:"
msgstr "ଏହି %s ଉପରେ:"

msgid "N/A"
msgstr "N/A"

msgid "No Alerts"
msgstr "କୌଣସି ସତର୍କ ସୂଚନା ନାହିଁ"

msgid "SELinux has detected a problem."
msgstr "SELinux ଗୋଟିଏ ସମସ୍ୟାକୁ ଚିହ୍ନିପାରିଛି।"

msgid "Sealert Error"
msgstr "Sealert ତ୍ରୁଟି"

msgid "Sealert Message"
msgstr "Sealert ସନ୍ଦେଶ"

#. -----------------------------------------------------------------------------
msgid "signature not found"
msgstr "ହସ୍ତାକ୍ଷର ମିଳିଲା ନାହିଁ"

msgid "multiple signatures matched"
msgstr "ଏକାଧିକ ହସ୍ତାକ୍ଷର ମିଶିଗଲା"

msgid "id not found"
msgstr "ପରିଚୟ ମିଳିଲା ନାହିଁ"

msgid "database not found"
msgstr "ତଥ୍ଯାଧାର ମିଳିଲା ନାହିଁ"

msgid "item is not a member"
msgstr "ବସ୍ତୁ ଗୋଟିଏ ସଦସ୍ଯ ନୁହେଁ"

msgid "illegal to change user"
msgstr "ଚାଳକ ବଦଳାଇବା ଅବୈଧ ଅଟେ"

msgid "method not found"
msgstr "ପଦ୍ଧତି ମିଳିଲା ନାହିଁ"

msgid "cannot create GUI"
msgstr "GUI ସୃଷ୍ଟି କରିପାରିବ ନାହିଁ"

msgid "value unknown"
msgstr "ମୂଲ୍ଯ ଜଣାନାହିଁ"

msgid "cannot open file"
msgstr "ଫାଇଲ ଖୋଲିପାରିବ ନାହିଁ"

msgid "invalid email address"
msgstr "ଅବୈଧ email ଠିକଣା"

#. gobject IO Errors
msgid "socket error"
msgstr "ସକେଟ ତ୍ରୁଟି"

msgid "connection has been broken"
msgstr "ସଂଯୋଗ ଛିନ୍ନ ହୋଇଗଲାଣି"

msgid "Invalid request. The file descriptor is not open"
msgstr "ଅବୈଧ ଅନୁରୋଧ। ଫାଇଲ ନିରୂପକଟି ଖୋଲାନାହିଁ"

msgid "insufficient permission to modify user"
msgstr "ଚାଳକ ପରିବର୍ତ୍ତନ କରିବାପାଇଁ ଅପର୍ୟାପ୍ତ ଅନୁମତି"

msgid "authentication failed"
msgstr "ବୈଧିକରଣ ବିଫଳ"

msgid "user prohibited"
msgstr "ଚାଳକ ନିଷିଦ୍ଧ"

msgid "not authenticated"
msgstr "ବୈଧିକରଣ କରାଯାଇନାହିଁ"

msgid "user lookup failed"
msgstr "ଚାଳକ ଅବଲୋକନ ବିଫଳ"

#, fuzzy, c-format, python-format
#| msgid "Opps, %s hit an error!"
msgid "Oops, %s hit an error!"
msgstr "ଓହୋ, %s ଗୋଟିଏ ତୃଟିକୁ ଦବାଇଲା"

msgid "Error"
msgstr "ତୃଟି"

msgid ""
"If you want to allow $SOURCE_BASE_PATH to have $ACCESS access on the "
"$TARGET_BASE_PATH $TARGET_CLASS"
msgstr ""

#, python-format
msgid " For complete SELinux messages run: sealert -l %s"
msgstr ""

#, python-format
msgid "The user (%s) cannot modify data for (%s)"
msgstr "(%s) ପାଇଁ ଚାଳକ (%s) ତଥ୍ୟପରିବର୍ତ୍ତନ କରିପାରିବନାହିଁ"

msgid "Started"
msgstr "ଆରମ୍ଭ ହେଲା"

msgid "AVC"
msgstr "AVC"

msgid "Audit Listener"
msgstr "ଶ୍ରୋତା ପରୀକ୍ଷଣ"

msgid "Never Ignore"
msgstr "କଦାପି ଆଗ୍ରହ୍ଯ କରନ୍ତୁ ନାହିଁ"

msgid "Ignore Always"
msgstr "ସବୁବେଳେ ଏଡାନ୍ତୁ"

msgid "Ignore After First Alert"
msgstr "ପ୍ରଥମ ସତର୍କତା ପରେ ଏଡାଇଯାଆନ୍ତୁ"

msgid "directory"
msgstr "ଡିରେକ୍ଟୋରି"

msgid "semaphore"
msgstr "semaphore"

msgid "shared memory"
msgstr "ସହଭାଗୀ ସ୍ମୃତିସ୍ଥାନ"

msgid "message queue"
msgstr "ସଂଦେଶ କ୍ରମ"

msgid "message"
msgstr "ସଂଦେଶ"

msgid "file"
msgstr "ଫାଇଲ"

msgid "socket"
msgstr "ସକେଟ"

msgid "process"
msgstr "ପଦ୍ଧତି"

msgid "process2"
msgstr ""

msgid "filesystem"
msgstr "ଫାଇଲ ତନ୍ତ୍ର"

msgid "node"
msgstr "ନୋଡ"

msgid "capability"
msgstr "କ୍ଷମତା"

msgid "capability2"
msgstr ""

#, python-format
msgid "%s has a permissive type (%s). This access was not denied."
msgstr "%s ରେ ଗୋଟିଏ ଅନୁମୋଦିତ ପ୍ରକାର ଅଛି (%s)। ଏହି ଅଭିଗମ୍ୟତାକୁ ବାରଣ କରାହୋଇନାହିଁ।"

msgid "SELinux is in permissive mode. This access was not denied."
msgstr "SELinux ଅନୁମୋଦିତ ଅବସ୍ଥାରେ ଅଛି। ଏହି ଅଭିଗମ୍ୟତାକୁ ବାରଣ କରାହୋଇନାହିଁ।"

#, python-format
msgid "SELinux is preventing %s from using the %s access on a process."
msgstr "SELinux %s କୁ ଏକ ପ୍ରକ୍ରିୟାରେ %s କୁ ବ୍ୟବହାର କରିବାରୁ ବାରଣ କରିଥାଏ।"

#, python-format
msgid "SELinux is preventing %s from using the '%s' accesses on a process."
msgstr "SELinux %s କୁ ଏକ ପ୍ରକ୍ରିୟାରେ '%s' କୁ ବ୍ୟବହାର କରିବାରୁ ବାରଣ କରିଥାଏ।"

#, python-format
msgid "SELinux is preventing %s from using the %s capability."
msgstr "SELinux %s କୁ ଏକ ପ୍ରକ୍ରିୟାରେ %s କ୍ଷମତାକୁ ବ୍ୟବହାର କରିବାରୁ ବାରଣ କରିଥାଏ।"

#, python-format
msgid "SELinux is preventing %s from using the '%s' capabilities."
msgstr "SELinux %s କୁ ଏକ ପ୍ରକ୍ରିୟାରେ '%s' କ୍ଷମତାକୁ ବ୍ୟବହାର କରିବାରୁ ବାରଣ କରିଥାଏ।"

#, python-format
msgid "SELinux is preventing %s from %s access on the %s labeled %s."
msgstr "SELinux %s କୁ %s ପ୍ରବେଶାନୁମତି ପାଇଁ %s ନାମକ %s ଉପରେ ବାରଣ କରିଛି।"

#, python-format
msgid "SELinux is preventing %s from '%s' accesses on the %s labeled %s."
msgstr ""

#, python-format
msgid "SELinux is preventing %s from %s access on the %s %s."
msgstr "SELinux %s କୁ %s ଅଭିଗମ୍ୟରୁ %s %s ଉପରେ ବାରଣ କରିଥାଏ।"

#, python-format
msgid "SELinux is preventing %s from '%s' accesses on the %s %s."
msgstr "SELinux %s କୁ '%s' ଅଭିଗମ୍ୟରୁ %s %s ଉପରେ ବାରଣ କରିଥାଏ।"

msgid "Additional Information:\n"
msgstr "ଅତିରିକ୍ତ ସୂଚନା:\n"

msgid "Source Context"
msgstr "ଉତ୍ସ ପ୍ରସଙ୍ଗ"

msgid "Target Context"
msgstr "ଲକ୍ଷ୍ଯ ପ୍ରସଙ୍ଗ"

msgid "Target Objects"
msgstr "ଲକ୍ଷ୍ଯ ବସ୍ତୁ"

msgid "Source"
msgstr "ଉତ୍ସ"

msgid "Source Path"
msgstr "ଉତ୍ସ ପଥ"

msgid "Port"
msgstr "ସଂଯୋଗିକୀ"

msgid "Host"
msgstr "ଆଧାର"

msgid "Source RPM Packages"
msgstr "ଉତ୍ସ RPM ପ୍ଯାକେଜମାନ"

msgid "Target RPM Packages"
msgstr "ଲକ୍ଷ୍ୟ RPM ପ୍ଯାକେଜ ମାନ"

msgid "SELinux Policy RPM"
msgstr ""

msgid "Local Policy RPM"
msgstr ""

msgid "Selinux Enabled"
msgstr "Selinux ସାମର୍ଥିକରଣ ଥିବା"

msgid "Policy Type"
msgstr "ନୀତି ପ୍ରକାର"

msgid "Enforcing Mode"
msgstr "ବଳପୂର୍ବକ ଧାରା"

msgid "Host Name"
msgstr "ଆଧାର ନାମ"

msgid "Platform"
msgstr "ପ୍ଲାଟଫର୍ମ"

msgid "Alert Count"
msgstr "ସତର୍କ ଗଣନା"

msgid "First Seen"
msgstr "ପ୍ରଥମ ସାକ୍ଷାତ"

msgid "Local ID"
msgstr "ସ୍ଥାନିୟ ପରିଚୟ"

msgid "Raw Audit Messages"
msgstr "ପ୍ରକୃତ ସମୀକ୍ଷଣ ସନ୍ଦେଶ"

#, python-format
msgid ""
"\n"
"\n"
"*****  Plugin %s (%.4s confidence) suggests   "
msgstr ""
"\n"
"\n"
"*****  ପ୍ଲଗଇନ %s (%.4s ବିଶ୍ୱାସ) ପ୍ରସ୍ଥାବ ଦେଇଥାଏ"

msgid "*"
msgstr "*"

msgid "\n"
msgstr "\n"

msgid ""
"\n"
"Then "
msgstr ""
"\n"
"ତେବେ"

msgid ""
"\n"
"Do\n"
msgstr ""
"\n"
"କରନ୍ତୁ\n"

msgid ""
"\n"
"\n"
msgstr ""
"\n"
"\n"

msgid "New SELinux security alert"
msgstr "ନୂତନ SELinux ସୁରକ୍ଷା ଚେତାବନୀ"

msgid "AVC denial, click icon to view"
msgstr "AVC ଅସ୍ୱୀକରଣ, ଦେଖିବାପାଇଁ ଚିତ୍ରସଙ୍କେତକୁ ଦବାନ୍ତୁ"

msgid "Dismiss"
msgstr "ଅସ୍ୱୀକାର କରନ୍ତୁ"

msgid "Show"
msgstr "ଦର୍ଶାନ୍ତୁ"

#. set tooltip
msgid "SELinux AVC denial, click to view"
msgstr "SELinux AVC ଅସ୍ୱୀକରଣ, ଦେଖିବାପାଇଁ ଦବାନ୍ତୁ"

msgid "SELinux Troubleshooter: Applet requires SELinux be enabled to run"
msgstr ""

msgid "SELinux not enabled, sealert will not run on non SELinux systems"
msgstr "SELinux ସକ୍ରିୟ ନାହିଁ, sealert SELinux ହୋଇନଥିବା ତନ୍ତ୍ରଗୁଡ଼ିକୁ ଚଲାଇବ ନାହିଁ"

msgid "Not fixable."
msgstr "ଠିକ ହେବା ଯୋଗ୍ୟ ନୁହଁ।"

#, c-format
msgid "Successfully ran %s"
msgstr "ସଫଳତାର ସହିତ %s କୁ ଚଲାଉଛି"

#, c-format
msgid "Plugin %s not valid for %s id"
msgstr "ପ୍ଲଗଇନ %s %s id ପାଇଁ ବୈଧ ନୁହଁ"

msgid "SELinux not enabled, setroubleshootd exiting..."
msgstr "SELinux କୁ ସକ୍ରିୟ କରାଯାଇନାହିଁ, setroubleshootd ପ୍ରସ୍ଥାନ କରୁଅଛି..."

#, c-format
msgid "fork #1 failed: %d (%s)"
msgstr "ଫର୍କ #1 ବିଫଳହୋଇଛି: %d (%s)"

msgid ""
"Copyright (c) 2010\n"
"Thomas Liu <tliu@redhat.com>\n"
"Máirín Duffy <duffy@redhat.com>\n"
"Daniel Walsh <dwalsh@redhat.com>\n"
"John Dennis <jdennis@redhat.com>\n"
msgstr ""

msgid "Troubleshoot selected alert"
msgstr "ବଚ୍ଛିତ ସତର୍କ ସୂଚନାକୁ ତ୍ରୁଟିନିବାରଣ କରନ୍ତୁ"

msgid "Delete"
msgstr ""

msgid "Delete Selected Alerts"
msgstr "ବଚ୍ଛିତ ସତର୍କ ସୂଚନାଗୁଡ଼ିକୁ ଅପସାରଣ କରନ୍ତୁ"

msgid "Close"
msgstr ""

msgid "<b>SELinux has detected a problem.</b>"
msgstr "<b>SELinux ଗୋଟିଏ ସମସ୍ୟାକୁ ଚିହ୍ନିପାରିଛି।</b>"

msgid "Turn on alert pop-ups."
msgstr "ସତର୍କ ସୂଚନା ପପଅପଗୁଡ଼ିକୁ ଅନ କରନ୍ତୁ।"

msgid "Turn off alert pop-ups."
msgstr "ସତର୍କ ସୂଚନା ପପଅପଗୁଡ଼ିକୁ ବନ୍ଦ କରନ୍ତୁ।"

msgid "On this file:"
msgstr "ଏହି ଫାଇଲରେ:"

msgid "label"
msgstr "ନାମପଟି"

msgid ""
"Read alert troubleshoot information.  May require administrative privileges "
"to remedy."
msgstr "ସତର୍କ ସୂଚନା ତ୍ରୁଟିନିବାରଣ ସୂଚନାକୁ ପଢ଼ନ୍ତୁ।  ଚିକିତ୍ସା ପାଇଁ ପ୍ରଶାସନିକ ଅଧିକାର ଆବଶ୍ୟକ ହୋଇପାରେ।"

msgid "Email alert to system administrator."
msgstr "ତନ୍ତ୍ର ପ୍ରଶାସକ ପାଇଁ ଇମେଲ ସତର୍କତା।"

msgid "Delete current alert from the database."
msgstr "ଚଳିତ ଚେତାବନୀକୁ ତଥ୍ୟାଧାରରୁ ବିଲୋପ କରନ୍ତୁ।"

msgid "Previous"
msgstr ""

msgid "Show previous alert."
msgstr "ପୂର୍ବବର୍ତ୍ତି ସତର୍କ ସୂଚନାକୁ ଦର୍ଶାନ୍ତୁ।"

msgid "Next"
msgstr ""

msgid "Show next alert."
msgstr "ପରବର୍ତ୍ତି ସତର୍କ ସୂଚନାକୁ ଦର୍ଶାନ୍ତୁ।"

msgid "List all alerts in the database."
msgstr "ସମସ୍ତ ସତର୍କ ସୂଚନାକୁ ତଥ୍ୟାଧାରରେ ତାଲିକାଭୁକ୍ତ କରନ୍ତୁ।"

msgid "Review and Submit Bug Report"
msgstr "ତ୍ରୁଟି ବିବରଣୀ ଯାଞ୍ଚକରି ଦାଖଲ କରନ୍ତୁ"

msgid "<span size='large' weight='bold'>Review and Submit Bug Report</span>"
msgstr ""
"<span size='large' weight='bold'>ତ୍ରୁଟି ଖବରକୁ ପୁନରୂକ୍ଷଣ କରନ୍ତୁ ଏବଂ ଦାଖଲ କରନ୍ତୁ</span>"

msgid ""
"You may wish to review the error output that will be included in this bug "
"report and modify it to exclude any sensitive data below."
msgstr ""
"ଆପଣ ତ୍ରୁଟି ଫଳାଫଳକୁ ସମୀକ୍ଷା କରିପାରନ୍ତି ଯାହାକି ଏହି ତ୍ରୁଟି ବିବରଣୀରେ ଅନ୍ତର୍ଭୁକ୍ତ ଅଛି ଏବଂ ନିମ୍ନରେଥିବା "
"ଯେକୌଣସି ସମ୍ବେଦନଶୀଳ ତଥ୍ୟକୁ ବହିର୍ଭୁକ୍ତ କରିବା ପାଇଁ ତାହାକୁ ପରିବର୍ତ୍ତନ କରିପାରିବେ।"

msgid "Included error output:"
msgstr "ଅନ୍ତର୍ଭୁକ୍ତ ତ୍ରୁଟି ଫଳାଫଳ:"

msgid "Submit Report"
msgstr "ବିବରଣୀ ଦାଖଲ କରନ୍ତୁ"

msgid ""
"This operation was completed.  The quick brown fox jumped over the lazy dog."
msgstr ""
"ଏହି ପ୍ରୟୋଗଟି ସମ୍ପୂର୍ଣ୍ଣ ହୋଇଥିଲା।  ସେହି ତୀବ୍ର ବାଦାମୀ କୋକୀଶିଆଳୀ ଅଳସୁଆ କୁକୁର ଉପର ଦେଇ ଡେଇଁଗଲା।"

msgid "Success!"
msgstr "ସଫଳତା!"

msgid "button"
msgstr "ବଟନ"
